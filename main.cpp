#include <iostream>
#include "map.h"

int main() {
    map item_price;

    item_price.insert("apple",1);
    item_price.insert("steak",15);
    item_price.insert("pear",3);
    item_price.insert("banana",6);
    item_price.print();
    item_price.erase("banana");
    item_price.replace("pear",5);

    try{
        item_price.get("cheese");
    }catch(const char* reason)
    {
        std::cout << reason;
    }

    item_price["steak"]=20;
    std::cout << "\n\n";
    item_price.print();
    std::cout << "\n\n";
    item_price.print();

    std::cout << "\n\nthe price of a steak is $"<< item_price["steak"] << std::endl;

    return 0;
}