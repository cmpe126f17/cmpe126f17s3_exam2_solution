#include "gtest/gtest.h"
#include "../lib/exam02/inc/map.h"

class TestFixture : public ::testing::Test {
protected:
    virtual void TearDown() {
    }

    virtual void SetUp() {
        empty = new map();
        filled = new map();
        filled->insert(std::string("Apple"), 1);
        filled->insert(std::string("Grape"), 4);
        filled->insert(std::string("Banana"), 3);
        filled->insert(std::string("Orange"), 1);
        filled->insert(std::string("Pear"), 5);
        filled->insert(std::string("Watermelon"), 2);
        filled->insert(std::string("Peach"), 2);
        filled->insert(std::string("Strawberry"), 9);
        filled->insert(std::string("Guava"), 3);
    }

public:
    TestFixture() : Test() {
    }

    virtual ~TestFixture() {

    }
    map *empty, *filled;

};

TEST_F(TestFixture, insert) {
    empty->insert(std::string("Test"), 100);
    EXPECT_EQ(empty->get("Test"), 100);
    empty->insert(std::string("Test"), 50);
    EXPECT_EQ(empty->get("Test"), 100);
}

TEST_F(TestFixture, get){
    EXPECT_ANY_THROW(empty->get("Test"));
    empty->insert(std::string("Test"), 100);
    EXPECT_EQ(empty->get("Test"), 100);

    EXPECT_ANY_THROW(filled->get("Test"));
    EXPECT_EQ(filled->get("Grape"), 4);
}

TEST_F(TestFixture, brackets){
    EXPECT_ANY_THROW( (*empty)[std::string("Test")]);
    empty->insert(std::string("Test"), 100);
    EXPECT_EQ( (*empty)[std::string("Test")], 100);

    EXPECT_ANY_THROW( (*filled)[std::string("Test")]);
    EXPECT_EQ( (*filled)[std::string("Orange")], 1);
}

TEST_F(TestFixture, replace){
    empty->insert(std::string("Test"), 100);
    EXPECT_EQ(empty->get("Test"), 100);
    empty->replace(std::string("Test"), 50);
    EXPECT_EQ(empty->get("Test"), 50);
}

TEST_F(TestFixture, erase){
    EXPECT_EQ(filled->get(std::string("Watermelon")), 2);
    EXPECT_EQ(filled->erase(std::string("Watermelon")), true);
    EXPECT_ANY_THROW(filled->get(std::string("Watermelon")));
    EXPECT_EQ(filled->get(std::string("Banana")), 3);
}

TEST_F(TestFixture, print){
    testing::internal::CaptureStdout();
    empty->print();
    std::string output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, std::string(""));

    empty->insert(std::string("Test"), 100);
    testing::internal::CaptureStdout();
    empty->print();
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, std::string("Test: 100\n"));

    testing::internal::CaptureStdout();
    filled->print();
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, std::string("Guava: 3\nStrawberry: 9\nPeach: 2\nWatermelon: 2\nPear: 5\nOrange: 1\nBanana: 3\nGrape: 4\nApple: 1\n"));
}

TEST_F(TestFixture, clear){
    testing::internal::CaptureStdout();
    filled->print();
    std::string output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, std::string("Guava: 3\nStrawberry: 9\nPeach: 2\nWatermelon: 2\nPear: 5\nOrange: 1\nBanana: 3\nGrape: 4\nApple: 1\n"));

    filled->clear();
    testing::internal::CaptureStdout();
    filled->print();
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, std::string(""));
}