#ifndef CMPE126F17_EXAM2_NODE_H
#define CMPE126F17_EXAM2_NODE_H
#include <string>

class node {
public:
    node* next;
    std::string key;
    int value;

    explicit node(std::string key_in, int value_in) : key(key_in), value(value_in), next(nullptr) {}
};


#endif //CMPE126F17_EXAM2_NODE_H
