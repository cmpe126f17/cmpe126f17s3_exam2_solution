#include "map.h"
#include <iostream>

map::map()
{
    head= nullptr;
}

map::~map()
{
    node* current=head;

    while(current!= nullptr){
        node* next = current->next;
        delete current;
        current = next;
    }
}

void map::insert(std::string key, int value)
{
    if (head== nullptr){
        head = new node(key,value);
    }
    else if (!key_exist(key)){
        node* tmp= new node(key,value);
        tmp->next =head;
        head = tmp;
    }
}

void map::replace(std::string key, int value)
{
    if(!key_exist(key)){
        insert(key,value);
    }else
    {
        (*this)[key]=value;
    }
}

int map::get(std::string key)
{
    return (*this)[key];
}

bool map::erase(std::string key)
{
    if(head == nullptr || !key_exist(key))return false;

    node * iter_behind=head;
    node * iter = head->next;
    while(iter != nullptr && iter->key!=key)
    {
        iter = iter->next;
        iter_behind = iter_behind->next;
    }
    if(iter== nullptr)return false;

    iter_behind->next=iter->next;
    delete iter;
    return true;
}

void map::print() {
    node *iter = head;
    while (iter != nullptr) {
        std::cout << iter->key << ": " << iter->value << std::endl;
        iter=iter->next;
    }
}

void map::clear()
{
    node* current=head;

    while(current!= nullptr){
        node* next = current->next;
        delete current;
        current = next;
    }
    head = nullptr;
}

int& map::operator[](std::string key)
{
    node* iter=head;
    while(iter!= nullptr && iter->key!= key)
        iter=iter->next;

    if(iter == nullptr) throw "Key not found";
    return iter->value;
}

bool map::key_exist(std::string key) {
    node* iter = head;
    while(iter != nullptr && iter->key !=key)
        iter=iter->next;
    return iter != nullptr;
}